
public class SwapBot {

	final static int numPoints = 100;
	
	public static void main(String[] args) {
		if(args.length < 1)
			return;
		System.out.print(new SwapBot().run(args[0]));
	}
	
	String run(String input){
		String[] tokens = input.split(",");
		if(tokens.length < numPoints*2)
			return "0";
		Point[] points = new Point[numPoints];	
		for(int i=0;i<numPoints;i++)
			points[i] = new Point(Integer.valueOf(tokens[i*2+1]), Integer.valueOf(tokens[i*2+2]));

		int startDist = totalDistance(points);
		for(int i=0;i<numPoints;i++){
			for(int j=i+1;j<numPoints;j++){
				Point[] test = copyPoints(points);
				Point tmp = test[i];
				test[i] = test[j];
				test[j] = tmp;
				if(totalDistance(test) < startDist)
					return getPathString(test);
			}
		}
		return "0";
	}
	
	String getPathString(Point[] in){
		String path = "";
		for(int i=0;i<numPoints;i++){
			path += in[i].x + "," + in[i].y;
			if(numPoints-i>1)
				path += ",";
		}
		return path;
	}

	Point[] copyPoints(Point[] in){
		Point[] out = new Point[in.length];
		for(int i=0;i<out.length;i++)
			out[i] = new Point(in[i]);
		return out;
	}
	
	static int totalDistance(Point[] in){
		int dist = 0;
		for(int i=0;i<numPoints;i++){
			dist += in[i].distance(in[(i+1)%numPoints]);
		}
		return dist;
	}

	class Point{
		int x;
		int y;
		
		Point(int x, int y){
			this.x = x;
			this.y = y;
		}
		
		Point(Point other){
			this.x = other.x;
			this.y = other.y;
		}
		
		boolean equals(Point other){
			if(this.x==other.x && this.y==other.y)
				return true;
			return false;
		}
		
		int distance(Point other){
			return Math.abs(x-other.x) + Math.abs(y-other.y);
		}
	}
}
