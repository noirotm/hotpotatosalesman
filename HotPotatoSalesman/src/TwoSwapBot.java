public class TwoSwapBot {

	static int numPoints = 100;
	
	String run(String input){
		String[] tokens = input.split(",");
		if(tokens.length < numPoints*2)
			return "bad input? nope. no way. bye.";
		
		Point[] points = new Point[numPoints];	
		for(int i=0;i<numPoints;i++)
			points[i] = new Point(Integer.valueOf(tokens[i*2+1]), Integer.valueOf(tokens[i*2+2]));
		int startDist = totalDistance(points);

		Point[][] swapped = new Point[(numPoints*(numPoints+1))/2][];		
		int idx = 0;
		for(int i=0;i<numPoints;i++){
			for(int j=i+1;j<numPoints;j++){
				Point[] test = copyPoints(points);
				swapPoints(test,i,j);
				int testDist = totalDistance(test);
				if( testDist < startDist)
					return getPathString(test);
				else
					swapped[idx++] = test;
			}
		}
		
		for(int i=0;i<idx;i++){
			for(int k=0;k<numPoints;k++){
				for(int l=k+1;l<numPoints;l++){
					swapPoints(swapped[i],k,l);
					if(totalDistance(swapped[i]) < startDist)
						return getPathString(swapped[i]);
					swapPoints(swapped[i],k,l);
				}
			}
		}
		return "well damn";
	}
	
	void swapPoints(Point[] in, int a, int b){
		Point tmp = in[a];
		in[a] = in[b];
		in[b] = tmp;
	}
	
	String getPathString(Point[] in){
		String path = "";
		for(int i=0;i<numPoints;i++)
			path += in[i].x + "," + in[i].y + ",";
		return path.substring(0,path.length()-1);
	}

	Point[] copyPoints(Point[] in){
		Point[] out = new Point[in.length];
		for(int i=0;i<out.length;i++)
			out[i] = new Point(in[i].x, in[i].y);
		return out;
	}
	
	static int totalDistance(Point[] in){
		int dist = 0;
		for(int i=0;i<numPoints-1;i++)
			dist += in[i].distance(in[i+1]);
		return dist + in[numPoints-1].distance(in[0]);
	}

	public static void main(String[] args) {
		if(args.length < 1)
			return;
		System.out.print(new TwoSwapBot().run(args[0]));
	}
	
	class Point{
		final int x; final int y;
		Point(int x, int y){this.x = x;	this.y = y;}
		int distance(Point other){return Math.abs(x-other.x) + Math.abs(y-other.y);}
	}
}