require 'rasem'

class Point
  attr_reader :x, :y
  
  def initialize(x, y)
    @x, @y = x, y
  end
end

# entry point
values = ARGV[0].split(',')

# initialize point array
points = []
while !values.empty?
  points << Point.new(values.shift.to_i, values.shift.to_i)
end

# initialize point array
img = Rasem::SVGImage.new(840,840) do
  points.each_with_index do |p,i|
    np = points[(i+1) % points.size]
    line p.x*4+10, p.y*4+10, np.x*4+10, np.y*4+10
    
    text p.x*4+15, p.y*4+15, i.to_s
    
    with_style(:fill=>"red") do
      circle p.x*4+10, p.y*4+10, 3
      circle np.x*4+10, np.y*4+10, 3
    end
  end
end

IO.write("output.svg", img.output)
