import java.io.IOException;
import java.util.Random;

public class Game {

	static boolean verbose = false;
	final static int numPoints = 100;
	final static int threshold = 75;
	final static int size = 200;
	final static int BAD = -1;

	Player[] players;
	Random rand;
	Point[] points;
	int currentScore;
	Point[] currentPath;
	
	public static void main(String[] args){
		if(args.length < 2){
			System.out.println("That command will get you nowhere. Add a couple players. I mean... c'mon.");
			return;
		}
		Player[] playersIn = new Player[args.length];
		int[] order = new int[args.length];
		for(int i=0;i<args.length;i++){
			playersIn[i] = new Player(args[i], i);
			order[i] = i;
		}
		verbose = true;
		int winner = new Game(playersIn, order, new Random()).run();
		System.out.println("(" + playersIn[winner].id + ") " + playersIn[winner].name + " wins!");
	}
	
	public Game(Player[] playersIn, int[] order, Random rand){
		this.rand = rand;
		players = new Player[playersIn.length];
		for(int i=0;i<order.length;i++)
			players[i] = playersIn[order[i]];
	}
	
	int run(){
		init();
		
		int turn = 0;
		while(livePlayers() > 1){
			Player player = players[turn % players.length];
			if(!player.alive){
				turn++;
				continue;
			}
			String path = getPathString(currentPath);
			path = livePlayers() + "," + path;
			long start = System.currentTimeMillis();
			String newPath = player.takeTurn(path);
			if(player.alive){
				if(parsePath(newPath) == BAD)
					player.alive = false;
			}
			if(player.alive){
				log(currentScore + " :\t\t (" + player.id + ") " + player.name + "\t" + (System.currentTimeMillis()-start) + " ms");
			} else {
				log("(" + player.id + ") " + player.name + " is out!" + "\t" + (System.currentTimeMillis()-start) + " ms");
				log(path);
			}
			
			try {
				Runtime.getRuntime().exec("ruby draw_path.rb " + newPath);
			}
			catch (IOException e) {
				log(e);
			}
			
			turn++;
		}
		for(Player player : players)
			if(player.alive)
				return player.id;
		return -1;
	}
	
	int livePlayers(){
		int live = 0;
		for(Player player : players)
			if(player.alive)
				live++;
		return live;
	}
	
	void init(){
		createPoints();
		currentScore = totalDistance(points);
		currentPath = points; 
		for(int i=0;i<players.length;i++)
			players[i].alive = true;
		log(currentScore + " :\t New Game Started");
	}
	
	void createPoints(){
		boolean[][] taken = new boolean[size][size];
		points = new Point[numPoints];
		for(int i=0;i<numPoints;i++){
			int x = rand.nextInt(size);
			int y = rand.nextInt(size);
			while(taken[x][y]==true){
				x = rand.nextInt(size);
				y = rand.nextInt(size);
			}
			points[i] = new Point(x,y);
			taken[x][y] = true;			
		}
	}
	
	int totalDistance(Point[] in){
		int dist = 0;
		for(int i=0;i<numPoints;i++){
			dist += in[i].distance(in[(i+1)%numPoints]);
		}
		return dist;
	}
	
	String getPathString(Point[] in){
		String path = "";
		for(int i=0;i<numPoints;i++){
			path += in[i].x + "," + in[i].y;
			if(numPoints-i>1)
				path += ",";
		}
		return path;
	}
	
	int parsePath(String input){
		String[] tokens = input.split(",");
		if(tokens.length < numPoints*2){
			log("malformed output");
			return BAD;
		}
		Point[] test = new Point[numPoints];
		int same = 0;
		try {
			for(int i=0;i<numPoints;i++){
				test[i] = new Point(Integer.valueOf(tokens[i*2]), Integer.valueOf(tokens[i*2+1]));
				if(test[i].equals(currentPath[i]))
					same++;
			}
			if(same < threshold){
				log("only " + same + " matching points");
				return BAD;
			}
		} catch (NumberFormatException e) {
			log("non-integer values in response");
			return BAD;
		}
		if(!compareLists(test, points)){
			log("incorrect point list");
			return BAD;
		}
		int dist = totalDistance(test);
		if(dist >= currentScore){
			log("path not shorter");
			return BAD;
		}
		
		currentPath = test;
		currentScore = dist;
		return dist;
	}
	
	boolean compareLists(Point[] a, Point[] b){
		int matches = 0;
		for(int i=0;i<numPoints;i++){
			for(int j=0;j<numPoints;j++){
				if(a[i].equals(b[j]))
					matches++;
			}
		}
		return matches == numPoints;
	}
	
	static <T>void log(T msg){
		if(verbose)
			System.out.println(msg);
	}
	
	class Point{
		int x;
		int y;
		
		public Point(int x, int y){
			this.x = x;
			this.y = y;
		}
		
		public boolean equals(Point other){
			if(x==other.x && y==other.y)
				return true;
			return false;
		}
		
		public int distance(Point other){
			return Math.abs(x-other.x) + Math.abs(y-other.y);
		}
	}
	
}
