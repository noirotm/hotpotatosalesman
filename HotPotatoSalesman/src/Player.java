import java.io.InputStream;
import java.util.Timer;
import java.util.TimerTask;

public class Player {

	static final int timeout = 60 * 1000;

	String cmd;
	String name;
	int id;
	int tourneyScore;
	boolean alive;
	
	public Player(String cmd, int id){
		this.cmd = cmd;
		this.id = id;
		tourneyScore = 0;
		String[] tokens = cmd.split(" ");
		name = tokens[tokens.length-1];
	}
	
	String takeTurn(String path){
		String[] tokens = cmd.split(" ");
		String[] args = new String[tokens.length+1];
		for(int i=0;i<tokens.length;i++)
			args[i] = tokens[i];
		args[args.length-1] = path;
//		System.out.println(path);
		
		try{
			final Process process = new ProcessBuilder(args).redirectErrorStream(true).start();
			InputStream input = process.getInputStream();
			byte[] buffer = new byte[1024];		
			
			Timer timer = new Timer();
			timer.schedule(new TimerTask() {
				@Override
				public void run() {
					process.destroy();
				}
			}, timeout);
			
			byte in;
			int index = 0;
			while ((in = (byte) input.read()) > 0) {
				buffer[index++] = in;
			}
			String out = new String(buffer, 0, index, "US-ASCII");
//			System.out.println(out);
			timer.cancel();
			process.destroy();
			return out;
			
		}catch(Exception e){
			e.printStackTrace();
			alive = false;
			return "";
		}
	}
}
