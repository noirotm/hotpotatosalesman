import java.util.Random;

public class Tourney {

	final static long seed = 1;			// will change seed for official run
	final static Random rand = new Random(seed);
	
	Player[] players;
	int numGames;
	int[][] ordering;
		
	// Each contestant is an argument. 
	// e.g.  "python potatoPasser.py" or "./compiledPasser"
	public static void main(String[] args) {
		Tourney tourney = new Tourney();
		if(tourney.loadPlayers(args) < 2){
			System.out.println("Believe it or not, you need at least two contestants to run a tourney. Really.");
			return;
		}
		tourney.init();
		tourney.run();
	}
	
	void run(){
		System.out.println("Starting tournament with seed " + seed + "\n");
		for(int i=0;i<numGames;i++){
			int winner = new Game(players, ordering[i], rand).run();
			players[winner].tourneyScore++;
			System.out.println("(" + players[winner].id + ") " + players[winner].name + " wins a game! Current score: " + players[winner].tourneyScore);
			
		}
		System.out.println("\nFinal Results:\n");
		System.out.println("Wins\t\tContestant");
		for(int p=0;p<players.length;p++){
			System.out.println(players[p].tourneyScore + "\t\t(" + players[p].id + ") " + players[p].name);
		}
		
	}
	
	int loadPlayers(String[] args){
		players = new Player[args.length];
		int count = 0;
		for(String arg : args){
			Player player = new Player(arg, count++);
			players[player.id] = player;
		}
		return players.length;
	}
	
	void init(){
		numGames = players.length * 5;
		ordering = new int[numGames][];
		int[] order = new int[players.length];
		for(int i=0;i<order.length;i++)
			order[i] = i;
		for(int i=0;i<numGames;i++)
			ordering[i] = shuffleWithHead(order, i%players.length);
		
	}
	
	int[] shuffleWithHead(int[] in, int head){
		int[] out = new int[in.length];
		for(int i=0;i<in.length;i++)
			out[i] = in[i];
		for(int i=0;i<out.length;i++){
			int j = rand.nextInt(out.length-i) + i;
			int t = out[i];
			out[i] = out[j];
			out[j] = t;
		}
		int headIndex = -1;
		for(int i=0;i<out.length;i++)
			if(out[i] == head)
				headIndex = i;
		if(headIndex >= 0){
			int t = out[0];
			out[0] = out[headIndex];
			out[headIndex] = t;
		}
		return out;
	}
	
}
